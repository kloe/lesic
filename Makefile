all: liblesic specification

.PHONY: liblesic
liblesic:
	patscc -c liblesic/lesic.dats

.PHONY: specification
specification:
	xsltproc                                                             \
		--xinclude                                                   \
		${DOCBOOK_XML_XSLT}/share/xml/docbook-xsl/xhtml/chunk.xsl    \
		specification/index.xml

.PHONY: install
install:
	mkdir -p ${out}/{liblesic,specification}
	install -t ${out}/liblesic *.o
	install -t ${out}/specification *.html
