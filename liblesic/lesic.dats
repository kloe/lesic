staload "./lesic.sats"

staload stdio = "libats/libc/SATS/stdio.sats"

dataviewtype handle_vt =
	| handle of ($stdio.FILEptr1($stdio.w()))
assume handle = handle_vt

implement close(h) =
	case+ h of
	| ~handle(file) =>
		let
			val file_ref = $stdio.FILEptr_refize(file)
			val _ = $stdio.fclose(file_ref)
		in
			()
		end
