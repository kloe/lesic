absviewtype handle = ptr

// FIXME: When fclose fails, we ignore the error. We should pass a FILEptr to
// FIXME: fclose instead of a FILEref, and return the error to the caller.
fun close(handle): void
