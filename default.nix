let
	pkgs = import <nixpkgs> {};
in
	pkgs.stdenv.mkDerivation {
		name = "lesic";
		src = ./.;
		buildInputs = [
			pkgs.ats2
			pkgs.docbook_xml_xslt
			pkgs.libxslt
		];
		DOCBOOK_XML_XSLT = pkgs.docbook_xml_xslt;
	}
